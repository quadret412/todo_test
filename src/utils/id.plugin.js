// this plugin generates new ID based on date
// I decided to put this function in a separate plugin to avoid code repeating

export default {
  install(Vue) {
    Vue.prototype.$genId = function() {
      return `${(+new Date).toString(16)}`
    }
  }
}