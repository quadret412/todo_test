import Vue from 'vue'
import App from './App.vue'
import router from './router'
import idPlugin from './utils/id.plugin'

export const eventEmitter = new Vue()

Vue.use(idPlugin)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
